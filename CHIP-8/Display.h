#pragma once

#include <SFML/Graphics.hpp>
#include "Types.h"

class Display
{
public:
	static const uint W = 64;
	static const uint H = 32;

	Display();

	void setPixel(byte x, byte y, bool on);
	void flipPixel(byte x, byte y);
	bool getPixel(byte x, byte y);

	bool drawByte(byte x, byte y, byte toDraw);

	void clear();

	void render(sf::RenderTarget &rt, const sf::Vector2f &origin);

private:
	byte constructMask(byte x);

	byte m_buffer[8][32];

	sf::RectangleShape m_borderRect, m_pixelRect;
};