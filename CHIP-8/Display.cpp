#include <cstdio>
#include "Display.h"

Display::Display()
{
	m_borderRect.setFillColor(sf::Color::Transparent);
	m_borderRect.setOutlineColor(sf::Color::White);
	m_borderRect.setOutlineThickness(1.0f);

	m_pixelRect.setFillColor(sf::Color::Cyan);
	m_pixelRect.setOutlineThickness(0.0f);

	for (auto &y : m_buffer) for (auto &x : y) x = 0;
}

void Display::setPixel(byte x, byte y, bool on)
{
	if (x >= W || y >= H) return;
	
	byte mask = constructMask(x);
	
	if(on) m_buffer[x / 8][y] |= mask;
	else m_buffer[x / 8][y] &= ~mask;
}

void Display::flipPixel(byte x, byte y)
{
	if (x >= W || y >= H) return;
	
	byte mask = constructMask(x);
	m_buffer[x / 8][y] ^= mask;
}

bool Display::getPixel(byte x, byte y)
{
	if (x >= W || y >= H) return false;
	
	byte mask = constructMask(x);
	return (m_buffer[x / 8][y] & mask) > 0;
}

byte Display::constructMask(byte x)
{
	uint bitIdx = x % 8;
	return (1 << (7 - bitIdx));
}

bool Display::drawByte(byte x, byte y, byte toDraw)
{
	bool collision = false;

	for (int i = 0; i < 8; i++)
	{
		byte mask = constructMask(i);
		byte xPos = (x + i) % W;

		bool bit = (toDraw & mask) > 0;
		bool pixel = getPixel(xPos, y);
		bool result = bit ^ pixel;

		setPixel(xPos, y, result);
		if (!result) collision = true;
	}

	return collision;
}

void Display::clear()
{
	for (auto &y : m_buffer) for (auto &x : y) x = 0;
}

void Display::render(sf::RenderTarget &rt, const sf::Vector2f &origin)
{
	static const float SCALE = 10.0f;

	m_borderRect.setSize(sf::Vector2f(W * SCALE, H * SCALE));
	m_borderRect.setPosition(origin);

	rt.draw(m_borderRect);

	m_pixelRect.setSize(sf::Vector2f(SCALE, SCALE));

	for (byte x = 0; x < W; x++)
	{
		for (byte y = 0; y < H; y++)
		{
			if (getPixel(x, y))
			{
				m_pixelRect.setPosition(origin.x + (float)x * SCALE, origin.y + (float)y * SCALE);
				rt.draw(m_pixelRect);
			}
		}
	}
}