#include <SFML/Graphics.hpp>
#include "Emulator.h"

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		printf("Usage: chip-8 ROMFILE\n");
		return -1;
	}

	sf::RenderWindow window(sf::VideoMode(672, 432), "CHIP-8", sf::Style::Close);
	//window.setFramerateLimit(60);

	srand(time(0));

	Emulator emu(argv[1]);

	sf::RectangleShape infoBox;
	infoBox.setSize(sf::Vector2f(640, 80));
	infoBox.setPosition(16.0f, 336.0f);
	infoBox.setFillColor(sf::Color::Transparent);
	infoBox.setOutlineColor(sf::Color::White);
	infoBox.setOutlineThickness(1.0f);

	sf::Font font;

	// TODO: Maybe switch this out for a monospace font.
	font.loadFromFile("res/pixelmix.ttf");

	sf::Text text;
	text.setFont(font);
	text.setCharacterSize(8u);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				emu.stop();
				window.close();
				break;

			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Space)
				{
					if (emu.isRunning()) emu.stop();
					else emu.start();
				}
				else if (event.key.code == sf::Keyboard::Tab)
				{
					if(emu.isRunning()) emu.stop();
					emu.tick();
				}
			}

			emu.doEvent(event);
		}

		if(emu.isRunning()) emu.tick();

		window.clear();

		emu.render(window, sf::Vector2f(16, 16));
		window.draw(infoBox);
		
		for (int x = 0; x < 4; x++)
		{
			for (int y = 0; y < 4; y++)
			{
				byte idx = byte(x + y * 4);

				char reg[] = "V0 = 0xFF";
				sprintf_s(reg, "V%X = 0x%02X", idx, emu.V[idx]);

				text.setString(reg);
				text.setPosition(infoBox.getPosition() + sf::Vector2f(float(8 + x * 64), float(8 + y * 16)));
				window.draw(text);
			}
		}

		// TODO: Write a disassembler that can give us human-readable instructions rather than just hex codes.
		char curPC[] = "PC = 0x0000";
		sprintf_s(curPC, "PC = 0x%04X", emu.PC());
		text.setString(curPC);
		text.setPosition(infoBox.getPosition() + sf::Vector2f(272, 8));
		window.draw(text);

		char curInstruction[] = "[PC] = 0x0000";
		sprintf_s(curInstruction, "[PC] = 0x%04X", emu.fetchNoModify());
		text.setString(curInstruction);
		text.setPosition(infoBox.getPosition() + sf::Vector2f(272, 24));
		window.draw(text);

		char dtState[] = "DT = 0x00";
		sprintf_s(dtState, "DT = 0x%02X", emu.DT());
		text.setString(dtState);
		text.setPosition(infoBox.getPosition() + sf::Vector2f(272, 40));
		window.draw(text);

		char stState[] = "ST = 0x00";
		sprintf_s(stState, "ST = 0x%02X", emu.ST());
		text.setString(stState);
		text.setPosition(infoBox.getPosition() + sf::Vector2f(272, 56));
		window.draw(text);

		text.setPosition(infoBox.getPosition() + sf::Vector2f(450, 8));
		text.setString("[Space] to pause/unpause.\n\n[Tab] to step forward one frame.");
		window.draw(text);

		// Draw the keypad.
		sf::RectangleShape keyRect;
		keyRect.setSize(sf::Vector2f(16.0f, 16.0f));
		keyRect.setOutlineColor(sf::Color::White);
		keyRect.setOutlineThickness(1.0f);

		byte keys[4][4] =
		{
			{ 0x1, 0x2, 0x3, 0xC },
			{ 0x4, 0x5, 0x6, 0xD },
			{ 0x7, 0x8, 0x9, 0xE },
			{ 0xA, 0x0, 0xB, 0xF },
		};

		for (int x = 0; x < 4; x++)
		{
			for (int y = 0; y < 4; y++)
			{
				byte key = keys[y][x];

				keyRect.setPosition(infoBox.getPosition() + sf::Vector2f(350 + x * keyRect.getSize().x, 8.0f + y * keyRect.getSize().y));

				if (emu.keyboard().isPressed(key)) keyRect.setFillColor(sf::Color::Cyan);
				else keyRect.setFillColor(sf::Color::Transparent);

				window.draw(keyRect);

				char keyId[] = "0";
				sprintf_s(keyId, "%X", key);
				text.setString(keyId);
				text.setPosition(keyRect.getPosition() + sf::Vector2f(4.0f, 4.0f));
				window.draw(text);
			}
		}

		window.display();
	}

	return 0;
}