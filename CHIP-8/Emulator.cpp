#include <fstream>
#include <iostream>
#include "Emulator.h"

Emulator::Emulator(const std::string &romFilename) :
//V0(0), V1(0), V2(0), V3(0), V4(0), V5(0), V6(0), V7(0), V8(0), V9(0), VA(0), VB(0), VC(0), VD(0), VE(0), VF(0),
m_soundTimer(0), m_delayTimer(0),
m_programCounter(0x200),
m_stackPointer(0),
I(0),
m_running(false),
m_waitingForKey(false),
m_keyWaitRegister(0)
{
	for (auto & n : V) n = 0;

	std::ifstream file(romFilename, std::ios::binary);
	if (!file.is_open())
	{
		std::cerr << "Rom not found, Invalid file name?" << std::endl;
		return;
	}

	// Digit sprites.
	byte digits[16][5] =
	{
		{ 0xF0, 0x90, 0x90, 0x90, 0xF0 },	// 0
		{ 0x20, 0x60, 0x20, 0x20, 0x70 },	// 1
		{ 0xF0, 0x10, 0xF0, 0x80, 0xF0 },	// 2
		{ 0xF0, 0x10, 0xF0, 0x10, 0xF0 },	// 3
		{ 0x90, 0x90, 0xF0, 0x10, 0x10 },	// 4
		{ 0xF0, 0x80, 0xF0, 0x10, 0xF0 },	// 5
		{ 0xF0, 0x80, 0xF0, 0x90, 0xF0 },	// 6
		{ 0xF0, 0x10, 0x20, 0x40, 0x40 },	// 7
		{ 0xF0, 0x90, 0xF0, 0x90, 0xF0 },	// 8
		{ 0xF0, 0x90, 0xF0, 0x10, 0xF0 },	// 9
		{ 0xF0, 0x90, 0xF0, 0x90, 0x90 },	// A
		{ 0xE0, 0x90, 0xE0, 0x90, 0xE0 },	// B
		{ 0xF0, 0x80, 0x80, 0x80, 0xF0 },	// C
		{ 0xE0, 0x90, 0x90, 0x90, 0xE0 },	// D
		{ 0xF0, 0x80, 0xF0, 0x80, 0xF0 },	// E
		{ 0xF0, 0x80, 0xF0, 0x80, 0x80 },	// F
	};

	for (int i = 0; i < 16; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			m_RAM[i * 5 + j] = digits[i][j];
		}
	}

	// Get size of the file.
	file.seekg(0, std::ios::end);
	u32 fileSize = (u32)file.tellg();
	file.seekg(0, std::ios::beg);

	// Load the file's bytes into RAM, starting at address 0x200.
	file.read((char *)&m_RAM[0x200], fileSize);
	/*for (u32 i = 0x200; i < 0x200 + fileSize; i += 2)
	{
		u16 value;
		file.read((char *)&value, 2);

		m_RAM[i] = (value & 0xFF00) >> 8;
		m_RAM[i + 1] = (value & 0x00FF);
	}*/

	file.close();

	//m_running = true;	// ROM loaded properly, so we can start executing.
}

void Emulator::tick()
{
	if (m_waitingForKey)
	{
		byte pressedKey;

		if (m_keyboard.anyKeyPressed(pressedKey))
		{
			V[m_keyWaitRegister] = pressedKey;
			m_waitingForKey = false;
		}
		else return;
	}

	if (m_delayTimer > 0) m_delayTimer--;
	if (m_soundTimer > 0) m_soundTimer--;

	u16 instruction = fetch();

	// Convert from big endian.
	instruction = ((instruction & 0xFF00) >> 8) | ((instruction & 0x00FF) << 8);

	// Shifting it right by 12 means we move that nibble down to the least significant bits, so we can check like this:
	// case 0xF		(Matches 0xF*** instructions)
	switch ((instruction & 0xF000) >> 12)
	{
	case 0x0:	// Collection of instructions (weird ones...)
	{
		switch ((instruction & 0x00FF))
		{
		case 0xE0:	// CLS
			m_display.clear();
			break;

		case 0xEE:	// RET
			m_stackPointer--;
			m_programCounter = m_stack[m_stackPointer];
			break;

		default:
			std::cout << "00 Instruction not yet implemented: " << std::hex << instruction << std::dec << std::endl;
			m_running = false;
			break;
		}

		break;
	}
	case 0x1:	// JP nnn
	{
		u16 nnn = instruction & 0x0FFF;
		m_programCounter = nnn;
		break;
	}
	case 0x2:	// CALL nnn
	{
		u16 nnn = (instruction & 0x0FFF);

		m_stack[m_stackPointer] = m_programCounter;
		m_stackPointer += 1;

		m_programCounter = nnn;

		break;
	}
	case 0x3:	// SE Vx, kk
	{
		byte x = (instruction & 0x0F00) >> 8;
		byte kk = instruction & 0x00FF;

		if (V[x] == kk) m_programCounter += 2;
		
		break;
	}
	case 0x4: // SNE Vx, kk
	{
		byte x = (instruction & 0x0F00) >> 8;
		byte kk = instruction & 0x00FF;

		if (V[x] != kk) m_programCounter += 2;
		
		break;

	}
	case 0x5:
	{
		byte x = (instruction & 0x0F00) >> 8;
		byte y = (instruction & 0x00F0) >> 4;
		if (V[x] == V[y]) m_programCounter += 2;

		break;
	}
	case 0x6:	// LD Vx, kk
	{
		byte x = (instruction & 0x0F00) >> 8;
		byte kk = instruction & 0x00FF;

		V[x] = kk;

		break;
	}
	case 0x7:	// ADD Vx, kk
	{
		byte x = (instruction & 0x0F00) >> 8;
		byte kk = instruction & 0x00FF;
		V[x] = V[x] + kk;

		break;
	}
	case 0x8:	// Collection of instructions operating values x, y
	{
		byte x = (instruction & 0x0F00) >> 8;
		byte y = (instruction & 0x00F0) >> 4;

		switch (instruction & 0x000F)
		{
		case 0x0:	// LD Vx, Vy
			V[x] = V[y];
			break;

		case 0x1:
			V[x] |= V[y];
			break;

		case 0x2:	// AND Vx, Vy
			V[x] &= V[y];
			break;

		case 0x3:	// XOR Vx, Vy
			V[x] ^= V[y];
			break;

		case 0x4:	// ADD Vx, Vy
		{
			u16 result = V[x] + V[y];
			V[x] = result & 0xFF;
			if (result > 255) V[0xF] = 1; else V[0xF] = 0;
			break;
		}
		case 0x5:	// SUB Vx, Vy
			if (V[x] > V[y]) V[0xF] = 1; else V[0xF] = 0;
			V[x] = V[x] - V[y];
			break;

		case 0x6:	// SHR Vx
			if ((V[x] & 0x01) > 0) V[0xF] = 1; else V[0xF] = 0;
			V[x] >>= 1;
			break;

		case 0x7:	// SUBN Vx, Vy
			if (V[y] > V[x]) V[0xF] = 1; else V[0xF] = 0;
			V[x] = V[y] - V[x];
			break;

		case 0xE:
			if ((V[x] & 0x80) > 0) V[0xF] = 1; else V[0xF] = 0;
			V[x] <<= 1;
			break;

		default:
			std::cout << "8xy Instruction not yet implemented: " << std::hex << instruction << std::dec << std::endl;
			m_running = false;
			break;
		}

		break;
	}
	case 0x9:	// SNE Vx, Vy
	{
		byte x = (instruction & 0x0F00) >> 8;
		byte y = (instruction & 0x00F0) >> 4;
		if (V[x] != V[y]) m_programCounter += 2;

		break;
	}

	case 0xA:	// LD I, nnn
	{
		u16 nnn = (instruction & 0x0FFF);

		I = nnn;
		break;
	}
	case 0xC: // RND Vx, kk
	{
		byte x = (instruction & 0x0F00) >> 8;
		byte kk = (instruction & 0x00FF);
		byte randomByte = rand();

		V[x] = kk & randomByte;
		break;

	}
	case 0xD:	// DRW Vx, Vy, n
	{
		byte x = (instruction & 0x0F00) >> 8;
		byte y = (instruction & 0x00F0) >> 4;
		byte n = (instruction & 0x000F);

		bool collision = false;

		for (int i = 0; i < n; i++)
		{
			byte toDraw = m_RAM[I + i];

			if (m_display.drawByte(V[x], (V[y] + i) % m_display.H, toDraw)) collision = true;
		}
		if (collision) V[0xF] = 1; else V[0xF] = 0;

		break;
	}
	case 0xE: // Collection of instructions that use keyboard input
	{
		byte x = (instruction & 0x0F00) >> 8;

		switch (instruction & 0x00FF)
		{
		case 0x9E:	// SKP Vx
			if (m_keyboard.isPressed(V[x])) m_programCounter += 2;
			break;

		case 0xA1:	// SKNP Vx
			if (!m_keyboard.isPressed(V[x])) m_programCounter += 2;
			break;

		default:
			std::cout << "Ex Instruction not yet implemented: " << std::hex << instruction << std::dec << std::endl;
			m_running = false;
			break;
		}

		break;
	}
	case 0xF:	// Collection of instructions using x.
	{
		byte x = (instruction & 0x0F00) >> 8;

		switch (instruction & 0x00FF)
		{
		case 0x07:	// LD Vx, DT
			V[x] = m_delayTimer;
			break;

		case 0x0A:	// LD Vx, K
			m_waitingForKey = true;
			m_keyWaitRegister = x;
			break;

		case 0x15:	// LD DT, Vx
			m_delayTimer = V[x];
			break;

		case 0x18:	// LD ST, Vx
			m_soundTimer = V[x];
			break;

		case 0x1E:	// I += Vx
			I += V[x];
			break;

		case 0x29:	// LD F, Vx		(F == spront)
		{
			byte digit = V[x];
			I = (u16)digit * 5;

			break;
		}
		case 0x33:	// LD B, Vx		(B = BCD)
		{
			byte VX = V[x];

			byte hundreds = 0x00;
			byte tens = 0x00;
			byte digits = 0x00;
			
			hundreds = VX / 100;
			VX -= hundreds * 100;

			tens = VX / 10;
			VX -= tens * 10;

			digits = VX;

			m_RAM[I] = hundreds;
			m_RAM[I + 1] = tens;
			m_RAM[I + 2] = digits;

			break;
		}
		case 0x55:	// LD [I], V[0-x]
			for (int i = 0; i <= x; i++) m_RAM[I + i] = V[i];
			break;

		case 0x65:	// LD V[0-x], [I]
			for (int i = 0; i <= x; i++) V[i] = m_RAM[I + i];
			break;

		default:
			std::cout << "Fx Instruction not yet implemented: " << std::hex << instruction << std::dec << std::endl;
			m_running = false;
			break;
		}
		break;
	}

	default:
		std::cout << "Instruction not yet implemented: " << std::hex << instruction << std::dec << std::endl;
		m_running = false;
		break;
	}
}

u16 Emulator::fetch()
{
	u16 instruction = memRead<u16>(m_programCounter);

	m_programCounter += sizeof(instruction);

	return instruction;
}

u16 Emulator::fetchNoModify() const
{
	return memRead<u16>(m_programCounter);
}

void Emulator::doEvent(const sf::Event &event)
{
	switch (event.type)
	{
	case sf::Event::KeyPressed:
		m_keyboard.press(event.key.code);
		break;

	case sf::Event::KeyReleased:
		m_keyboard.release(event.key.code);
		break;
	}
}

void Emulator::render(sf::RenderTarget &rt, const sf::Vector2f &origin)
{
	m_display.render(rt, origin);
}