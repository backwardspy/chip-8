#pragma once

typedef unsigned int u32;
typedef signed int s32;
typedef unsigned short u16;
typedef signed short s16;
typedef unsigned char u8;
typedef signed char s8;
typedef u32 uint;
typedef u16 ushort;
typedef u8 byte;